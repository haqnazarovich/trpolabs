﻿using System;

namespace TRPOLabs.ClassLibrary
{
    public class Сircle
    {
        public double AreaSectorCircle(double radius, double sectorAngle)
        {
            return Math.Round(Math.PI * Math.Pow(radius, 2) * sectorAngle / 360, 1);
        }

    }
}
