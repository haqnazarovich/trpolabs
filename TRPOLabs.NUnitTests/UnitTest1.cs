using NUnit.Framework;
using TRPOLabs.ClassLibrary;

namespace TRPOLabs.NUnitTests
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            const double radius = 30;
            const double sectorAngle = 20;
            const double expected = 157.1;

            var result = new �ircle().AreaSectorCircle(radius, sectorAngle);

            Assert.AreEqual(expected, result);

        }

        [Test]
        public void Test2()
        {
            const double radius = 47;
            const double sectorAngle = 45;
            const double expected = 867.5;

            var result = new �ircle().AreaSectorCircle(radius, sectorAngle);

            Assert.AreEqual(expected, result);

        }
    }
}