﻿using System;
using TRPOLabs.ClassLibrary;

namespace TRPOLabs.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Расчёт площадь сектора круга");

            Console.Write("Введите радиус круга: ");

            var radius = double.Parse(Console.ReadLine());

            Console.Write("Введите велечену дуги: ");

            var sectorAngle = double.Parse(Console.ReadLine());

            var result = new Сircle().AreaSectorCircle(radius, sectorAngle);

            Console.Write("Площадь сектора круга равна: " + result + " м^2");

            Console.ReadKey();
        }
    }
}
