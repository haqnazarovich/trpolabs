﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using TRPOLabs.ClassLibrary;
using TRPOLabs.WebApp.Models;

namespace TRPOLabs.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new СircleViewModel());
        }

        [HttpPost]
        public IActionResult Index(СircleViewModel сircleVM)
        {
            if (сircleVM.Radius > 360 || сircleVM.Radius < 0 || сircleVM.SectorAngle < 0 || сircleVM.Radius == null || сircleVM.SectorAngle == null)
            {
                сircleVM.Result = null;
                ViewBag.Eror = "Данные введены некорректно!";
            }

            else
                сircleVM.Result = new Сircle().AreaSectorCircle((double)сircleVM.Radius, (double)сircleVM.SectorAngle);

            return View(сircleVM);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
