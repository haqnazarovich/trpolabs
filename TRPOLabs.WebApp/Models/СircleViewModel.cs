﻿namespace TRPOLabs.WebApp.Models
{
    public class СircleViewModel
    {
       public double? Radius { get; set; }
        public double? SectorAngle { get; set; }
        public double? Result { get; set; }
    }
}
