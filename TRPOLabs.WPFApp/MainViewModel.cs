﻿using System.ComponentModel;
using TRPOLabs.ClassLibrary;

namespace TRPOLabs.WPFApp
{
    class MainViewModel : INotifyPropertyChanged
    {
        private double? _radius;
        public double? Radius
        {
            get { return _radius; }
            set
            {
                if(value <0 || value>360)
                {
                    _radius = value;
                    Eror = "Данные введены некорректно!";
                }
                else
                {
                    _radius = value;
                    OnPropertyChanged("Result_S");
                    OnPropertyChanged("Radius");
                    Eror = "";
                }

            }
        }

        private double? _sectorAngle;
        public double? SectorAngle
        {
            get { return _sectorAngle; }
            set
            {
                if (value < 0 )
                {
                    _sectorAngle = value;
                    Eror = "Данные введены некорректно!";
                }
                else
                {
                    _sectorAngle = value;
                    OnPropertyChanged("Result_S");
                    OnPropertyChanged("SectorAngle");
                    Eror = "";
                }
            }
        }

        public double? Result_S
        {
            get
            {
                if (Radius != null && SectorAngle != null)
                    return new Сircle().AreaSectorCircle((double)Radius, (double)SectorAngle);
                else
                    return null;
                
            }
        }

        private string _eror;
        public string Eror
        {
            get { return _eror; }
            set
            {
                _eror = value;
                OnPropertyChanged("Eror");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
